package com.hendisantika.springbootkafkaconsumerexample.entity;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-kafka-consumer-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 26/08/18
 * Time: 18.40
 * To change this template use File | Settings | File Templates.
 */
public class User {
    private String name;
    private String dept;

    public User() {
    }

    public User(String name, String dept) {

        this.name = name;
        this.dept = dept;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("User{");
        sb.append("name='").append(name).append('\'');
        sb.append(", dept='").append(dept).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
